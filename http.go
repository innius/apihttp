package apihttp

import (
	"bytes"
	"context"
	"encoding/json"
	"fmt"
	"io/ioutil"
	"log"
	"net/http"
	"reflect"

	"github.com/aws/aws-xray-sdk-go/xray"
	"gopkg.in/go-playground/validator.v9"
)

type appError struct {
	error
	Message string
	Code    int
}

// Handler wraps Handler func which returns an error
func Handler(f func(w http.ResponseWriter, r *http.Request) error) http.HandlerFunc {
	return func(w http.ResponseWriter, r *http.Request) {
		err := f(w, r)
		if err != nil {
			HandleError(w, r.Context(), err)
		}
	}
}

func InternalServerError(err error) error {
	return Error(err, http.StatusInternalServerError, "")
}

func Error(err error, status int, message string) error {
	if message == "" {
		message = http.StatusText(status)
	}
	return &appError{
		error:   err,
		Code:    status,
		Message: message,
	}
}

func Errorf(err error, status int, format string, a ...interface{}) error {
	return Error(err, status, fmt.Sprintf(format, a...))
}

func HandleError(w http.ResponseWriter, c context.Context, err error) {
	if err == nil {
		return
	}
	log.Printf("%+v", err)
	xray.AddError(c, err)
	if applicationError, ok := err.(*appError); ok {
		http.Error(w, applicationError.Message, applicationError.Code)
		return
	}
	http.Error(w, err.Error(), http.StatusInternalServerError)
}

func NotFoundError(w http.ResponseWriter, message string) error {
	http.Error(w, message, http.StatusNotFound)
	return nil
}

func BadRequest(w http.ResponseWriter, message string) error {
	http.Error(w, message, http.StatusBadRequest)
	return nil
}

// convenience func for returning a status code only
func NoContent(w http.ResponseWriter, status int) error {
	w.WriteHeader(status)
	return nil
}

func JSON(w http.ResponseWriter, status int, v interface{}) error {
	buf := &bytes.Buffer{}
	if err := json.NewEncoder(buf).Encode(v); err != nil {
		return err
	}
	w.Header().Set("Content-Type", "application/json; charset=utf-8")
	w.WriteHeader(status)
	_, err := w.Write(buf.Bytes())
	return err
}

var Validator = validator.New()

func Bind(r *http.Request, v interface{}) error {
	b, err := ioutil.ReadAll(r.Body)
	if err != nil {
		return err
	}

	if err := json.Unmarshal(b, v); err != nil {
		return err
	}

	val := reflect.ValueOf(v)
	if val.Kind() == reflect.Ptr && !val.IsNil() {
		val = val.Elem()
	}

	if val.Kind() == reflect.Struct {
		if err := Validator.Struct(v); err != nil {
			return err
		}
	}
	return nil
}