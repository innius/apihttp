module bitbucket.org/innius/apihttp

require (
	github.com/aws/aws-sdk-go v1.16.18 // indirect
	github.com/aws/aws-xray-sdk-go v0.9.4
	github.com/cihub/seelog v0.0.0-20170130134532-f561c5e57575 // indirect
	github.com/go-playground/locales v0.12.1 // indirect
	github.com/go-playground/universal-translator v0.16.0 // indirect
	github.com/leodido/go-urn v1.1.0 // indirect
	github.com/pkg/errors v0.8.1 // indirect
	gopkg.in/go-playground/validator.v9 v9.25.0
)
